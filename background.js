﻿var ENGLISH_MODE = true;
var links = document.getElementsByTagName("a");
var divs = document.getElementsByTagName("div");
var i;
var re_ems = new RegExp("(E[GL][0-9]+JP)");
var re_epacket = new RegExp("(RR[0-9]+JP)");

for(i=0;i<links.length;i++){
    if(links[i].innerHTML.match(re_ems) || links[i].innerHTML.match(re_epacket)){
	if(ENGLISH_MODE)
	    links[i].href = ("https://trackings.post.japanpost.jp/services/srv/search/direct?searchKind=S004&locale=en&reqCodeNo1=+" + RegExp.$1);
	else
	    links[i].href = ("https://trackings.post.japanpost.jp/services/srv/search/direct?searchKind=S004&locale=ja&reqCodeNo1=+" + RegExp.$1);
	links[i].removeAttribute("onClick");
	links[i].target = "_blank";
    }
}

for(i=0;i<divs.length;i++){
    if(divs[i].id == "shipTrack"){
	divs[i].parentNode.removeChild(divs[i]);
    }
}
